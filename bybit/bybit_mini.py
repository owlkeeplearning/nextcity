import json
import uuid
import websockets
import asyncio
from pybit.unified_trading import HTTP

api_key=""
api_secret=""
job_period = 50

class A:
    def __init__(self):
        self.all_data = {}

    async def fundingrate(self):
        session = HTTP(
            testnet=False,
            api_key=api_key,
            api_secret=api_secret,
        )
        self.all_data['mkt'] = {}
        for symbol in ['BTCUSDT', 'ETHUSDT', 'XRPUSDT']:
            b2 = session.get_funding_rate_history(
                category="linear",
                symbol=symbol,
                limit=1,
            )
            self.all_data['mkt'][symbol] = {}
            self.all_data['mkt'][symbol]['fundingRate'] = b2['result']['list'][0]['fundingRate']
        session = await websockets.connect('wss://stream.bybit.com/v5/public/linear')
        # subscribe BTCPERP
        await session.send(json.dumps({'req_id': str(uuid.uuid4()) ,'op': 'subscribe', 'args': ['tickers.BTCUSDT']}))
        await session.send(json.dumps({'req_id': str(uuid.uuid4()) ,'op': 'subscribe', 'args': ['tickers.ETHUSDT']}))
        await session.send(json.dumps({'req_id': str(uuid.uuid4()) ,'op': 'subscribe', 'args': ['tickers.XRPUSDT']}))
        while True:
            data = await session.recv()
            data = json.loads(data)
            mkt_data = self.all_data.get('mkt', {})
            try:
                data = data['data']
                symbol = data['symbol']
                symbol_data = mkt_data.get(symbol, {})
                if 'lastPrice' in data:
                    symbol_data['lastprice'] = data['lastPrice']
                if 'fundingRate' in data:
                    if data['fundingRate'] != '0.0001':
                        symbol_data['fundingRate'] = data['fundingRate']
                mkt_data[symbol] = symbol_data
            except:
                pass
            self.all_data['mkt'] = mkt_data
            # print(self.all_data)

    async def print_all_data(self):
        # print(self.all_data)
        self.all_data['ss_cells'] = [
            self.all_data['borrow'],
            [(k, v['lastprice'], v['fundingRate']) for k, v in self.all_data['mkt'].items()],
            [[symbol] + balance for symbol, balance in self.all_data['balance'].items()],
            self.all_data['positions']
        ]
        with open('all_data.json', 'w') as f:
            json.dump(self.all_data, f)

    async def borrow(self):
        session = HTTP(
            testnet=False,
            api_key=api_key,
            api_secret=api_secret,
        )
        vip_level_0 = 'No VIP'
        vip_level_1 = 'VIP-1'
        self.all_data['borrow'] = [None, None]
        rate_usdt = session.spot_margin_trade_get_vip_margin_data(currency='USDT', vipLevel=vip_level_0)['result']['vipCoinList'][0]['list'][0]['hourlyBorrowRate']
        rate_usdc = session.spot_margin_trade_get_vip_margin_data(currency='USDC', vipLevel=vip_level_0)['result']['vipCoinList'][0]['list'][0]['hourlyBorrowRate']
        self.all_data['borrow'][0] = ['vip_0', rate_usdt, rate_usdc]
        rate_usdt = session.spot_margin_trade_get_vip_margin_data(currency='USDT', vipLevel=vip_level_1)['result']['vipCoinList'][0]['list'][0]['hourlyBorrowRate']
        rate_usdc = session.spot_margin_trade_get_vip_margin_data(currency='USDC', vipLevel=vip_level_1)['result']['vipCoinList'][0]['list'][0]['hourlyBorrowRate']
        self.all_data['borrow'][1] = ['vip_1', rate_usdt, rate_usdc]

    async def balance(self):
        session = HTTP(
            testnet=False,
            api_key=api_key,
            api_secret=api_secret,
        )
        b1 = session.get_wallet_balance(accountType='UNIFIED', coin='USDC,USDT')
        # interesting fields:
        time = b1['time']
        b1 = b1['result']['list'][0]
        # totalEquity
        # accountMMRate
        # accountIMRate
        # time
        asset_data = self.all_data.get('asset', [])
        asset_data.append([time, b1['totalEquity'], b1['accountMMRate'], b1['accountIMRate']])
        self.all_data['asset'] = asset_data

    async def get_positions(self):
        session = HTTP(
            testnet=False,
            api_key=api_key,
            api_secret=api_secret,
        )
        self.all_data['positions'] = []
        pos_data = self.all_data['positions']
        for symbol in ['BTCUSDT', 'ETHUSDT', 'XRPUSDT']:
            b1 = session.get_positions(category="linear", symbol=symbol)
            pos = b1['result']['list'][0]
            pos_data.append([symbol, pos['positionValue'], pos['size']])

    async def wrap_func(self, func_src, local_job_period=0):
        if local_job_period == 0:
            local_job_period = job_period
        while True:
            try:
                await func_src()
            except Exception as ex:
                print(ex)
            await asyncio.sleep(local_job_period)

    async def collatoral_info(self):
        session = HTTP(
            testnet=False,
            api_key=api_key,
            api_secret=api_secret,
        )
        b1 = session.get_collateral_info()
        b1 = b1['result']['list']
        b1 = [i for i in b1 if i['currency'] in ['USDT', 'USDC']]
        self.all_data['balance'] = {}
        for item in b1:
            self.all_data['balance'][item['currency']] = [item['borrowAmount'], item['freeBorrowAmount'], item['hourlyBorrowRate']]
        # print(b1)

async def handler():
    a = A()
    tasks = []
    tasks.append(a.wrap_func(a.print_all_data))
    tasks.append(a.fundingrate())
    tasks.append(a.wrap_func(a.borrow))
    tasks.append(a.wrap_func(a.balance, local_job_period=600))
    tasks.append(a.wrap_func(a.get_positions))
    tasks.append(a.wrap_func(a.collatoral_info))
    return await asyncio.gather(*tasks)

if __name__ == '__main__':
    asyncio.run(handler())