import os
import json
import re
import requests
from selectolax.parser import HTMLParser, Node

class A:
    def __init__(self, host: str, alphabat_base: str, file_base: str):
        self.host = host
        self.alphabat_base = alphabat_base
        self.file_base = file_base

    def run(self):
        centres = list(self.generate_centres())
        centres.sort(key=lambda x: x[0])
        f = self.file_base + 'birmingham_recycling_centres.html'
        # if f file exist, delete
        if os.path.exists(f):
            os.remove(f)
        centres_code = json.dumps(centres)
        html = '''
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Birmingham Recycling Centres</title>
    <style type="text/css">
table, th, td {{
    border: 1px solid black;
}}
    </style>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        $(document).ready(function () {{
            var centres = {0};
            for (var i = 0; i < centres.length; i++) {{
                var c = centres[i];
                var tr = $('<tr></tr>');
                tr.append($('<td></td>').text(c[0]));
                tr.append($('<td></td>').text(c[1]));
                $('#content').append(tr);
            }}
        }});
    </script>
</head>
<body>
    <table>
        <thead>
            <tr><th>Date</th><th>Location</th></tr>
        </thead>
        <tbody id="content">
        </tbody>
    </table>
</body>
</html>
'''.format(centres_code)
        open(f, 'w').write(html)

    def generate_centres(self):
        headers = {
            "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:109.0) Gecko/20100101 Firefox/118.0"
        }
        for i in range(0, 4):
            url = self.host + self.alphabat_base + chr(i + ord('A'))
            print(url)
            s = requests.get(url, headers=headers)
            if s.status_code != 200:
                break
            tree = HTMLParser(s.text)
            n: Node
            for n in tree.css('div.page-content .list--rich a.list__link'):
                recyclingcentre_url = self.host + n.attributes["href"]
                r = requests.get(recyclingcentre_url, headers=headers)
                if r.status_code != 200:
                    break
                tree_r = HTMLParser(r.text)
                info = tree_r.css('dl.list--definition dt,dd')
                info2 = [m.text().strip() for m in info]
                if len(info2) == 10:
                    info2 = info2[5:7]
                    dd = info2[0]
                    # dd is in format "Mon dd/mm/yyyy"
                    # use regular expression to extract date and convert the format to yyyy-mm-dd Mon
                    m = re.match(r'(\w+) (\d+)/(\d+)/(\d+)', dd)
                    if m:
                        month = m.group(3)
                        day = m.group(2)
                        year = m.group(4)
                        info2[0] = f'{year}-{month}-{day} {m.group(1)}'
                    else:
                        print(f'cannot parse date {dd}')
                    yield info2
                else:
                    print(f'skip {info.text()}')

if __name__ == '__main__':
    host = 'https://www.birmingham.gov.uk'
    alphabat_base = '/directory/70/a_to_z/'
    file_base = 'web_static/'
    A(host, alphabat_base, file_base).run()